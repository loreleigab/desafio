<?php

use GoJumpers\Controllers\ViewsController;

$view = new ViewsController();

?>
<!-- Brand Logo -->
<a href="<?= SERVERURL ?>inicio" class="brand-link">
    <img src="<?= SERVERURL ?>views/dist/img/menu-go-jumpers.png" alt="GoJumpers Logo" class="brand-image elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">&nbsp;</span>
</a>

<!-- Sidebar -->
<div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="<?= SERVERURL ?>" class="nav-link" id="dashboard">
                    <i class="fas fa-home"></i>
                    <p>Home</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?= SERVERURL ?>categoria/inicio" class="nav-link" id="inicio">
                    <i class="far fa-circle"></i>
                    <p>Categorias</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?= SERVERURL ?>produto/inicio" class="nav-link" id="inicio">
                    <i class="fas fa-box-open"></i>
                    <p>Produtos</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->