<?php
use GoJumpers\Controllers\Produto\ProdutoController;

$produtoObj = new ProdutoController();

$produtos = $produtoObj->listarProduto();
?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= SERVERURL ?>produto/inicio">Administration Panel</a></li>
                </ol>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <?php foreach ($produtos as $produto): ?>
                <div class="col-md-3">
                <!-- Horizontal Form -->
                <div class="card card-info">
                    <!-- /.card-header -->
                    <div class="card-body box-profile">
                            <div class="text-center">
                                <?php if ($produto->imagem): ?>
                                    <img class="profile-user-img img-fluid img-circle"
                                         src="<?= SERVERURL ?>uploads/<?= $produto->imagem ?>"
                                         alt="">
                                <?php else : ?>
                                    <img class="profile-user-img img-fluid img-circle"
                                         src="<?= SERVERURL ?>views/dist/img/sem-imagem.png"
                                         alt="">
                                <?php endif; ?>
                            </div>

                            <h3 class="profile-username text-center"><?= $produto->nome ?></h3>

                            <p class="text-muted text-center"><?= $produto->descricao ?></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Preço</b> <a class="float-right">$<?= $produto->preco ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>SKU</b> <a class="float-right"><?= $produto->sku ?></a>
                                </li>
                            </ul>

                     </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <?php endforeach;?>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->