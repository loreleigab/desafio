<?php

use GoJumpers\Controllers\Categoria\CategoriaController;
use GoJumpers\Controllers\Produto\ProdutoController;

$categoriaObj = new CategoriaController();
$produtoObj = new ProdutoController();

$categorias = $categoriaObj->listarCategoria();

$id = $_GET['id'] ?? null;
if ($id) {
    $produto = $produtoObj->recuperarProduto($id);
}

?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Cadastro de produto</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Dados</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal formulario-ajax" method="POST" enctype="multipart/form-data" action="<?= SERVERURL ?>ajax/produtoAjax.php" role="form" data-form="<?= ($id) ? "update" : "save" ?>">
                        <input type="hidden" name="_method" value="<?= ($id) ? "edita" : "cadastra" ?>">
                        <?php if ($id): ?>
                            <input type="hidden" name="id" value="<?= $id ?>">
                        <?php endif; ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-2">
                                    <label for="sku">Produto SKU *</label>
                                    <input type="text" class="form-control" id="sku" name="sku"  pattern="[0-9]{4}-[0-9]{3}" maxlength="8" value="<?= $produto->sku ?? null ?>" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nome">Produto *</label>
                                    <input type="text" class="form-control" id="nome" name="nome" maxlength="20" value="<?= $produto->nome ?? null ?>" required>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="preco">Preço *</label>
                                    <input type="text" class="form-control" id="preco" name="preco" maxlength="20" value="<?= $produto->preco ?? null ?>" required>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="quantidade">Quantidade *</label>
                                    <input type="text" class="form-control" id="quantidade" name="quantidade" maxlength="20" value="<?= $produto->quantidade ?? null ?>" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="categoria" class="label">Categorias *</label>
                                    <select class="form-control select2bs4" name="categorias[]" multiple id="categoria" required>
                                        <option>Selecione...</option>
                                         <?php if ($categorias): ?>
                                            <?php foreach ($categorias as $categoria): ?>
                                                <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                            <?php endforeach; ?>
                                         <?php else: ?>
                                            <option value="" selected>Selecione uma categoria</option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="descricao" class="label">Descrição *</label>
                                    <textarea id="descricao" class="form-control" name="descricao" rows="3" required><?= $produto->descricao ?? null ?></textarea>
                                </div>
                            </div>
                            <!--<div class="row">
                                <div class="form-group col-md">
                                    <label for="exampleInputFile">Imagem</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Escolha o arquivo</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="<?= SERVERURL ?>produto/inicio">
                                <button type="button" class="btn btn-default pull-left">Voltar</button>
                            </a>
                            <button type="submit" class="btn btn-info float-right">Gravar</button>
                        </div>
                        <!-- /.card-footer -->
                        <div class="resposta-ajax"></div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>