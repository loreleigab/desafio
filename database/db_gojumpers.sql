-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para gojumpers
CREATE DATABASE IF NOT EXISTS `gojumpers` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gojumpers`;

-- Copiando estrutura para tabela gojumpers.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(20) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela gojumpers.categorias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Copiando estrutura para tabela gojumpers.categoria_produto
CREATE TABLE IF NOT EXISTS `categoria_produto` (
  `categoria_codigo` int(11) NOT NULL,
  `produto_sku` varchar(8) NOT NULL,
  KEY `categorias_categoria_produto_fk_idx` (`categoria_codigo`),
  KEY `produtos_categoria_produto_fk_idx` (`produto_sku`),
  CONSTRAINT `categorias_categoria_produto_fk` FOREIGN KEY (`categoria_codigo`) REFERENCES `categorias` (`codigo`),
  CONSTRAINT `produtos_categoria_produto_fk` FOREIGN KEY (`produto_sku`) REFERENCES `produtos` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela gojumpers.categoria_produto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria_produto` ENABLE KEYS */;

-- Copiando estrutura para tabela gojumpers.produtos
CREATE TABLE IF NOT EXISTS `produtos` (
  `sku` varchar(8) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `descricao` longtext NOT NULL,
  `quantidade` int(11) NOT NULL,
  `preco` float(6,2) NOT NULL,
  `imagem` varchar(100) DEFAULT '',
  PRIMARY KEY (`sku`),
  UNIQUE KEY `sku_UNIQUE` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela gojumpers.produtos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
