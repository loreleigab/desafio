<?php
namespace GoJumpers\Controllers\Categoria;

use GoJumpers\Models\MainModel;
use GoJumpers\Models\DbModel;

class CategoriaController extends MainModel
{
    /**
     * <p>Cadastro de categoria</p>
     * @param $dados
     * @return string
     */
    public function cadastrarCategoria($dados):string
    {
        unset($dados['_method']);
        $dados = MainModel::limpaPost($dados);

        $insert = DbModel::insert("categorias", $dados);
        if ($insert->rowCount() >= 1) {
            $id = $this->connection()->lastInsertId();
            $alerta = [
                'alerta' => 'sucesso',
                'titulo' => 'Categoria cadastrada!',
                'texto' => 'Dados cadastrados com sucesso!',
                'tipo' => 'success',
                'location' => SERVERURL . "categoria/inicio"
            ];
        } else {
            $alerta = [
                'alerta' => 'simples',
                'titulo' => 'Oops! Algo deu Errado!',
                'texto' => 'Falha ao salvar os dados no servidor, tente novamente mais tarde',
                'tipo' => 'error',
            ];
        }
        return MainModel::sweetAlert($alerta);
    }

    /**
     * <p>Edição de categoria</p>
     * @param $dados
     * @param $id
     * @return string
     */
    public function editarCategoria($dados, $id): string
    {
        $id = MainModel::decryption($id);
        unset($dados['_method']);
        unset($dados['id']);
        $dados = MainModel::limpaPost($dados);
        $update = DbModel::update('categorias', $dados, $id);

        if ($update->rowCount() >= 1 || DbModel::connection()->errorCode() == 0) {
            $alerta = [
                'alerta' => 'sucesso',
                'titulo' => 'Categoria alterada com sucesso!',
                'texto' => 'Dados alterados com sucesso!',
                'tipo' => 'success',
                'location' => SERVERURL . "categoria/inicio"
            ];
        } else {
            $alerta = [
                'alerta' => 'simples',
                'titulo' => 'Oops! Algo deu Errado!',
                'texto' => 'Falha ao salvar os dados no servidor, tente novamente mais tarde',
                'tipo' => 'error',
            ];
        }
        return MainModel::sweetAlert($alerta);
    }

    /**
     * <p>Lista para categoria</p>
     * @return array|false
     */
    public function listarCategoria()
    {
        return $this->lista("categorias");
    }

    /**
     * <p>Recupera um categoria através do código</p>
     * @param int|string $id
     * @return false|mixed|object
     */
    public function recuperarCategoria($id)
    {
        $id = MainModel::decryption($id);
        return $this->consultaSimples("SELECT * FROM categorias WHERE id = '$id'")->fetchObject();
    }
    /**
     * <p>Apagar categoria</p>
     * @param $id
     * @return string
     */
    public function apagarCategoria($id):string
    {
        $id = MainModel::decryption($id);
        $apagar = $this->deleteEspecial("categorias", "id", $id);
        if ($apagar->rowCount() >= 1){
            $alerta = [
                'alerta' => 'sucesso',
                'titulo' => 'Categoria apagada!',
                'texto' => 'Dados alterados com sucesso!',
                'tipo' => 'success',
                'location' => SERVERURL . 'categoria/inicio'
            ];
        } else {
            $alerta = [
                'alerta' => 'simples',
                'titulo' => 'Oops! Algo deu Errado!',
                'texto' => 'Falha ao salvar os dados no servidor, tente novamente mais tarde',
                'tipo' => 'error',
            ];
        }
        return MainModel::sweetAlert($alerta);
    }
}
