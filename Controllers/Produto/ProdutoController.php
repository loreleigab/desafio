<?php
namespace GoJumpers\Controllers\Produto;

use GoJumpers\Models\MainModel;
use GoJumpers\Models\DbModel;

class ProdutoController extends MainModel
{
    /**
     * <p>Cadastro de produto</p>
     * @param $dados
     * @return string
     */
    public function cadastrarProduto($dados):string
    {
        $categorias = $_POST['categorias'];
        $sku =  $_POST['sku'];
        unset($dados['_method']);
        unset($dados['categorias']);
        $dados = MainModel::limpaPost($dados);

        $insert = DbModel::insert("produtos", $dados);
        if ($insert->rowCount() >= 1) {

            MainModel::atualizaRelacionamento('categoria_produto', 'produto_sku', $sku, 'categoria_id', $categorias);
            $alerta = [
                'alerta' => 'sucesso',
                'titulo' => 'Produto cadastrado!',
                'texto' => 'Dados cadastrados com sucesso!',
                'tipo' => 'success',
                'location' => SERVERURL . "produto/inicio"
            ];
        } else {
            $alerta = [
                'alerta' => 'simples',
                'titulo' => 'Oops! Algo deu Errado!',
                'texto' => 'Falha ao salvar os dados no servidor, tente novamente mais tarde',
                'tipo' => 'error',
            ];
        }
        return MainModel::sweetAlert($alerta);
    }

    /**
     * <p>Edição de produto</p>
     * @param $dados
     * @param $id
     * @return string
     */
    public function editarProduto($dados, $id): string
    {
        $categorias = $_POST['categorias'];
        $sku =  $_POST['sku'];
        $id = MainModel::decryption($id);
        unset($dados['_method']);
        unset($dados['id']);
        unset($dados['categorias']);
        $dados = MainModel::limpaPost($dados);
        $update = DbModel::updateEspecial('produtos', $dados, "sku", $id);

        if ($update->rowCount() >= 1 || DbModel::connection()->errorCode() == 0) {
            MainModel::atualizaRelacionamento('categoria_produto', 'produto_sku', $sku, 'categoria_id', $categorias);
            $alerta = [
                'alerta' => 'sucesso',
                'titulo' => 'Produto alterado com sucesso!',
                'texto' => 'Dados alterados com sucesso!',
                'tipo' => 'success',
                'location' => SERVERURL . "produto/inicio"
            ];
        } else {
            $alerta = [
                'alerta' => 'simples',
                'titulo' => 'Oops! Algo deu Errado!',
                'texto' => 'Falha ao salvar os dados no servidor, tente novamente mais tarde',
                'tipo' => 'error',
            ];
        }
        return MainModel::sweetAlert($alerta);
    }

    /**
     * <p>Lista para produto</p>
     * @return array|false
     */
    public function listarProduto()
    {
        return $this->lista("produtos");
    }

    /**
     * <p>Recupera um produto através do código</p>
     * @param int|string $id
     * @return false|mixed|object
     */
    public function recuperarProduto($id)
    {
        $id = MainModel::decryption($id);
        return $this->consultaSimples("SELECT * FROM produtos WHERE sku = '$id'")->fetchObject();
    }

    public function recuperaCategoriaProduto($id, $selected = null)
    {
        $id = MainModel::decryption($id);
        $consulta = $this->consultaSimples("
            SELECT categoria_id as id, c.categoria
            FROM categoria_produto 
            INNER JOIN categorias c on categoria_produto.categoria_id = c.id
            WHERE produto_sku = '$id'
        ");
        if ($consulta->rowCount() >= 1) {
            $options = $consulta->fetchAll(\PDO::FETCH_NUM);
            foreach ($options as $option) {
                if ($option[0] == $selected) {
                    echo "<option value='" . $option[0] . "' selected >" . $option[1] . "</option>";
                } else {
                    echo "<option value='" . $option[0] . "'>" . $option[1] . "</option>";
                }
            }
        }
    }

    /**
     * <p>Apagar produto</p>
     * @param $id
     * @return string
     */
    public function apagarProduto($id):string
    {
        $id = MainModel::decryption($id);
        $apagar = $this->deleteEspecial("produtos", "id", $id);
        if ($apagar->rowCount() >= 1){
            $alerta = [
                'alerta' => 'sucesso',
                'titulo' => 'Produto apagada!',
                'texto' => 'Dados alterados com sucesso!',
                'tipo' => 'success',
                'location' => SERVERURL . 'produto/inicio'
            ];
        } else {
            $alerta = [
                'alerta' => 'simples',
                'titulo' => 'Oops! Algo deu Errado!',
                'texto' => 'Falha ao salvar os dados no servidor, tente novamente mais tarde',
                'tipo' => 'error',
            ];
        }
        return MainModel::sweetAlert($alerta);
    }
}
