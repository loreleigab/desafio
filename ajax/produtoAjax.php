<?php

use GoJumpers\Controllers\Produto\ProdutoController;

require_once "../config/configGeral.php";
require_once "../config/autoload_ajax.php";

if (isset($_POST['_method'])){
    $insertObj =  new ProdutoController();

    switch ($_POST['_method']){
        case 'cadastra':
            echo $insertObj->cadastrarProduto($_POST);
            break;
        case 'edita':
            echo $insertObj->editarProduto($_POST, $_POST['id']);
            break;
        case 'apaga':
            echo $insertObj->apagarProduto($_POST['id']);
            break;
        default:
            include_once "../config/destroySession.php";
            break;
    }

} else {
    include_once "../config/destroySession.php";
}
