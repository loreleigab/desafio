<?php

use GoJumpers\Controllers\Categoria\CategoriaController;

require_once "../config/configGeral.php";
require_once "../config/autoload_ajax.php";

if (isset($_POST['_method'])){
    $insertObj =  new CategoriaController();

    switch ($_POST['_method']){
        case 'cadastra':
            echo $insertObj->cadastrarCategoria($_POST);
            break;
        case 'edita':
            echo $insertObj->editarCategoria($_POST, $_POST['id']);
            break;
        case 'apaga':
            echo $insertObj->apagarCategoria($_POST['id']);
            break;
        default:
            include_once "../config/destroySession.php";
            break;
    }

} else {
    include_once "../config/destroySession.php";
}
